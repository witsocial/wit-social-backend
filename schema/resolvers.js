const User = require("../models/user");
const jwt = require("jsonwebtoken");
const Blog = require("../models/blog")


module.exports = resolvers = {
    Query: {
        getUsers: async () => {
            const users = await User.find();

            return users
        },
        getUser: () => {
            return { name: "anas", id: 3 }
        },
        blogs: async () => {
            try {
                const blogs= await Blog.find().populate("author")
                return blogs
                
            } catch (error) {
                console.log(error);
                throw error
            }
        }
    },
    Mutation: {
        createUser: async (_, args, { req }) => {
            console.log(req);
            const userData = args.input

            const newUser = new User({
                ...userData
            })
            const user = await newUser.save();

            return user
        },

        // create blog 

        createBlog: async (_, { input }) => {
            try {
                
                // const author = await User.findById(input.authorId);
                // if (!author) {
                //     throw new Error('User not found');
                // }

               
                const newBlog = await Blog.create({
                    title: input.title,
                    description: input.description,
                    author: input.author, 
                });

                return newBlog;
            } catch (error) {
                console.error('Error creating blog:', error.message);
                throw error;
            }
        },
        deleteBlog: async (_, { id }) => {
            try {
               
                await Blog.findByIdAndDelete(id);
                return id;
            } catch (error) {
                console.error( error.message);
                throw error;
            }
        },
        updateBlog: async (_, { id, input }) => {
            try {
               
                const author = await User.findById(input.authorId);
                if (!author) {
                    throw new Error('User not found');
                }

                
                const updatedBlog = await Blog.findByIdAndUpdate(
                    id,
                    {
                        title: input.title,
                        description: input.description,
                        author: author._id, 
                    },
                    { new: true } 
                );

                return updatedBlog;
            } catch (error) {
                console.error('Error updating blog:', error.message);
                throw error;
            }
        },


        // login resolver

        login: async (public, args, context) => {

            try {
                const userExists = await User.findOne({ email: args.input.email })

                if (!userExists) {
                    return new Error("Please register first")
                }
                if (args.input.password !== userExists.password) {
                    return new Error("Your password is incorrect")
                }
                const token = jwt.sign({ id: userExists._id }, "wdxasnxnxwnononklewbfyuvrkjbsdlcnioewjfioer")
                userExists.token = token
                await userExists.save();
                return userExists

            } catch (error) {
                console.log(error);
            }
        },
    }
}

module.exports= typedef=`

type user{
    name:String!
    email:String!
    password:String!
    id:ID!
    token:String
    role:String
}

type blog{
    id:ID!
    title:String!
    description:String!
    author:user
}
input createBlogInput{
    title:String
    description:String
    author:ID
}
type Query{
    getUsers:[user]
    getUser:user!
    blogs:[blog]
}
input UserInput{
    name:String!
    email:String!
    password:String!
}
input updateUser{
    name:String
    email:String
    password:String
    id:ID!
}
input loginInput{
    email:String
    password:String
}

type Mutation{
    createUser(input:UserInput):user
    updateUser(input:updateUser):user
    login(input:loginInput):user
    createBlog(input:createBlogInput):blog
    deleteBlog(id:ID):ID
    updateBlog(id:ID,input:createBlogInput):blog
}
`
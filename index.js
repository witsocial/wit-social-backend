const express = require("express")
const { ApolloServer } = require("@apollo/server")
const bodyParser = require("body-parser")
const cors = require("cors")
const { expressMiddleware } = require("@apollo/server/express4")
const app = express();

const typeDefs= require("./schema/typedef")
const resolvers = require("./schema/resolvers")
const mongoose = require("mongoose")

const serverStart = async () => {
    const server = new ApolloServer({
        typeDefs,
        resolvers,
        context:({req})=>{
            
            return {req}
        }
    });
    await server.start();
    app.use(cors({origin:true}))
    app.use(bodyParser.json());
    // app.use(
    //   bodyParser.urlencoded(),
    // );

    app.use("/graphql", expressMiddleware(server))

    const dataConnect = ()=>{
        mongoose.connect("mongodb+srv://anas:anas@cluster0.dqzsuz3.mongodb.net/witsocial?retryWrites=true&w=majority").then(()=>{
            console.log("database connencted");
        }).catch((err)=>{
            console.log(err);
        })
    }

    app.listen(8000, () => {
        dataConnect();
        console.log(`server is started`)

    })
}

serverStart();



